# Inicio Proyecto con Node JS

1. Iniciar proyecto
    ~~~
    npm init
    ~~~

2. Instalar modulos
    ~~~
    npm i express mongoose morgan dotenv cors express-validator
    ~~~

    - Express: Framework web para Node JS
    - Morgan: Permite observar en la consola las peticiones HTTP que van llegando a la API
    - Mongoose: ORM para MongoDB
    - Dotenv: Permite manejar variables de entorno
    - Cors: Habilita Cors
    - express-validator: Sirve para las validaciones

    ~~~
    npm i nodemon -D
    ~~~

    - Nodemon: Permite refrescar los cambios que se realizen en ambiente de desarrollo

3. Instalar modulos para tests
    - Jest: https://jestjs.io/
    - Supertest: https://github.com/visionmedia/supertest#readme
    
    - Ejecutar Tests
    ~~~
    npm run test
    ~~~

    - Ejecutar tests que incluyan cierta palabra
    ~~~
    npm run test -t "titan"
    ~~~

4. Datos de prueba
    - Crear datos de prueba
    ~~~
    node src/seed/seed.js -i
    ~~~

    - Limpiar datos de prueba
    ~~~
    node src/seed/seed.js -d
    ~~~