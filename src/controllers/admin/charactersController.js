const { response } = require("express")
const Character = require('../../models/Character')
const characterProps = 'id name lastname gender extract description imageUrl createdAt active'

const getCharacters = async(req, res = response) => {
  const options = {
    page: parseInt(req.query.page) || 1,
    limit: parseInt(req.query.limit) || 10
  }

  try {
    const docs = await Character.paginate({}, options)

    return res.status(200).json({
      success: true,
      characters: docs
    })
  } catch (error) {
    console.log(error)
    
    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const getCharacter = async(req, res = response) => {
  const id = req.params.id
  
  try {
    const characterDB = await Character.findById(id, characterProps)

    if (!characterDB) {
      return res.status(404).json({
        success: false,
        msg: 'Character not found'
      })
    }

    return res.status(200).json({
      success: true,
      data: characterDB
    })

  } catch (error) {
    console.log(error)
    
    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const createCharacter = async(req, res = response) => {
  const character = new Character(req.body)
  
  character.user = req.uid
  
  if (req.file) {
    const { filename } = req.file
    character.setImageUrl(filename)
  }

  try {
    await character.save()

    return res.status(201).json({
      id: character.id,
      success: true,
      msg: "Save success"
    })
  } catch (error) {
    console.log(error)

    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const updateCharacter = async(req, res = response) => {

  const id = req.params.id

  try {
    const character = await Character.findById(id)

    if (!character) {
      return res.status(404).json({
        success: false,
        msg: 'Character not found'
      })
    }

    const updateData = Object.assign({}, req.body)

    if (req.file) {
      const { filename } = req.file
      updateData.imageUrl = `${process.env.APP_HOST}:${process.env.PORT}/public/${filename}`
    }

    await Character.findByIdAndUpdate(id, updateData, { new: true })

    return res.status(200).json({
      success: true,
      msg: 'Character update success'
    })
  } catch (error) {
    console.log(error)

    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const deleteCharacter = async(req, res = response) => {
  const titanId = req.params.id

  try {
    const character = await Character.findById(titanId)

    if (!character) {
      return res.status(404).json({
        success: false,
        msg: 'Character not found'
      })
    }

    await Character.findByIdAndDelete(titanId)
    
    return res.status(200).json({
      success: true,
      msg: 'Character deleted'
    })
  } catch (error) {
    returnError(res)
  }
}

const returnError = (res) => {
  return res.status(500).json({
    success: false,
    msg: 'Server Error'
  })
}

module.exports = {
  getCharacters,
  createCharacter,
  getCharacter,
  updateCharacter,
  deleteCharacter
}