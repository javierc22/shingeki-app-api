const { response } = require("express")
const Titan = require('../../models/Titan')
const titanProps = 'id name userName extract height description imageUrl createdAt active'

const getTitans = async(req, res = response) => {
  const options = {
    page: parseInt(req.query.page) || 1,
    limit: parseInt(req.query.limit) || 10
  }

  try {
    const docs = await Titan.paginate({}, options)

    return res.status(200).json({
      success: true,
      titans: docs
    })
  } catch (error) {
    console.log(error)
    
    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const getTitan = async(req, res = response) => {
  const id = req.params.id
  
  try {
    const titanDB = await Titan.findById(id, titanProps)

    if (!titanDB) {
      return res.status(404).json({
        success: false,
        msg: 'Titan not found'
      })
    }

    return res.status(200).json({
      success: true,
      data: titanDB
    })

  } catch (error) {
    console.log(error)
    
    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const createTitan = async(req, res = response) => {
  const titan = new Titan(req.body)
  
  titan.user = req.uid
  
  if (req.file) {
    const { filename } = req.file
    titan.setImageUrl(filename)
  }

  try {
    await titan.save()

    return res.status(201).json({
      id: titan.id,
      success: true,
      msg: "Save success"
    })
  } catch (error) {
    console.log(error)

    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const updateTitan = async(req, res = response) => {

  const id = req.params.id

  try {
    const titan = await Titan.findById(id)

    if (!titan) {
      return res.status(404).json({
        success: false,
        msg: 'Titan not found'
      })
    }

    const updateData = Object.assign({}, req.body)

    if (req.file) {
      const { filename } = req.file
      updateData.imageUrl = `${process.env.APP_HOST}:${process.env.PORT}/public/${filename}`
    }

    await Titan.findByIdAndUpdate(id, updateData, { new: true })

    return res.status(200).json({
      success: true,
      msg: 'Titan update success'
    })
  } catch (error) {
    console.log(error)

    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const deleteTitan = async(req, res = response) => {
  const titanId = req.params.id

  try {
    const titan = await Titan.findById(titanId)

    if (!titan) {
      return res.status(404).json({
        success: false,
        msg: 'Titan not found'
      })
    }

    await Titan.findByIdAndDelete(titanId)
    
    return res.status(200).json({
      success: true,
      msg: 'Titan deleted'
    })
  } catch (error) {
    returnError(res)
  }
}

const returnError = (res) => {
  return res.status(500).json({
    success: false,
    msg: 'Server Error'
  })
}

module.exports = {
  getTitans,
  createTitan,
  getTitan,
  updateTitan,
  deleteTitan
}