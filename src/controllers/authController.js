const { response } = require('express')
const bcrypt = require('bcrypt')

const User = require('../models/User')
const { generateJWT } = require('../helpers/JWT')

const userProps = '_id name email'

const loginUser = async(req, res = response) => {
  const { email, password } = req.body

  try {
    // Validar usuario
    const user = await User.findOne({ email })

    if (!user) {
      return res.status(400).json({
        success: false,
        msg: 'Usuario o contraseña incorrectos'
      })
    }
    // Validar password
    const validPassword = bcrypt.compareSync(password, user.password)

    if (!validPassword) {
      return res.status(400).json({
        success: false,
        msg: 'Usuario o contraseña incorrectos'
      })
    }

    // Generar JWT
    const token = await generateJWT(user)

    return res.status(200).json({
      success: true,
      id: user.id,
      name: user.name,
      token
    })

  } catch (error) {
    console.log(error)
  }
}

const authenticatedUser = async(req, res = response) => {
  try {
    const user = await User.findById(req.uid, userProps)
    return res.status(200).json({
      success: true,
      user
    })
  } catch (error) {
    return res.status(500).json({
      msg: "Server error"
    })
  }
}

module.exports = {
  loginUser,
  authenticatedUser
}