const { response } = require('express')
const Character = require('../models/Character')

const characterProps = '_id name age gender'

const getCharacters = async(req, res = response) => {

  try {
    const characters = await Character.find({}, characterProps)

    return res.status(200).json({
      success: true,
      characters
    })
  } catch (error) {
    console.log(error)
    
    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}


const createCharacter = async(req, res = response) => {
  const character = new Character(req.body)

  try {
    await character.save()

    return res.status(200).json({
      success: true,
      msg: 'Save success'
    })
  } catch (error) {
    console.log(error)

    return res.status(500).json({
      success: false,
      msg: 'Server error'
    })
  }
}


const getCharacter = async(req, res = response) => {
  const id = req.params.id
  
  try {
    const characterDB = await Character.findById(id, characterProps)

    if (!characterDB) {
      return res.status(404).json({
        ok: false,
        msg: 'Character not found'
      })
    }

    return res.status(200).json({
      success: true,
      character: characterDB
    })

  } catch (error) {
    console.log(error)
    
    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const updateCharacter = async(req, res = response) => {
  const characterId = req.params.id

  try {
    const characterDB = await Character.findById(characterId)
    
    if (!characterDB) {
      return res.status(404).json({
        ok: false,
        msg: 'Character not found'
      })
    }

    await Character.findByIdAndUpdate(characterId, req.body, { new: true })

    return res.status(200).json({
      success: true,
      msg: 'Update success'
    })

  } catch (error) {
    console.log(error)

    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}


module.exports = {
  getCharacters,
  createCharacter,
  getCharacter,
  updateCharacter
}