// const { response } = require("express")
const Titan = require('../models/Titan')
const titanProps = 'name extract height description imageUrl'

const getTitans = async(req, res = response) => {
  try {
    const titans = await Titan.find({}, titanProps)

    return res.status(200).json({
      success: true,
      titans: titans
    })
  } catch (error) {
    console.log(error)
    
    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

const getTitan = async(req, res = response) => {
  const id = req.params.id
  console.log(id);
  
  try {
    const titanDB = await Titan.findById(id, titanProps)


    if (!titanDB) {
      return res.status(404).json({
        ok: false,
        msg: 'Titan not found'
      })
    }

    return res.status(200).json({
      success: true,
      titan: titanDB
    })

  } catch (error) {
    console.log(error)
    
    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

module.exports = {
  getTitans,
  getTitan
}