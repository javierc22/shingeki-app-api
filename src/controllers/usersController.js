const { response } = require('express')
const { encryptPassword } = require('../helpers/encryptPassword')
const User = require('../models/User')

const characterProps = '_id name lastname email'

const createUser = async(req, res = response) => {
  const { email, password } = req.body

  try {
    let user = await User.findOne({email})

    if(user) {
      return res.status(400).json({
        success: false,
        msg: 'Usuario ya existe con ese correo'
      })
    }

    user = new User(req.body)
    user.password = encryptPassword(password)

    await user.save()

    return res.status(200).json({
      success: true,
      msg: 'User created success'
    })

  } catch (error) {
    console.log(error)
    
    return res.status(500).json({
      success: false,
      msg: 'Server Error'
    })
  }
}

module.exports = {
  createUser
}
