const mongoose = require('mongoose')

const options = {
  useNewUrlParser: true, 
  useUnifiedTopology: true,
  useCreateIndex: true
}

const { MONGO_URI, MONGO_URI_TEST } = process.env

const dbConnection = () => {
  const connection = process.env.NODE_ENV === 'test' ? MONGO_URI_TEST : MONGO_URI
  try {
    mongoose.connect(connection, options)
    console.log("DB connected");
    
  } catch (error) {
    console.log(error)
    throw new Error('Error to connect Database')
  }
}

module.exports = {
  dbConnection
}