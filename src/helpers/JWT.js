const jwt = require('jsonwebtoken')

const generateJWT = (user) => {
  return new Promise( (resolve, reject) => {
    const payload = { uid: user.id }

    jwt.sign( payload, process.env.JWT_SECRET, {
      expiresIn: '1h'
    }, (err, token ) => {
      if ( err ){
        reject(err)
      }

      resolve(token)
    })
  })
}

module.exports = {
  generateJWT
}
