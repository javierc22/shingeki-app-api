const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
require('dotenv').config()

const { dbConnection } = require("./database/config")

// Crea servidor con Express
const app = express()

// Conectar base de datos
dbConnection()

// Lectura y parseo del body
app.use(express.json())

// Mostrar peticiones en consola
app.use(morgan('dev'))

// Habilita Cors
app.use(cors())

// Mapear 'public' por 'storage'
app.use('/public', express.static(`${__dirname}/storage/images`))

// Rutas
app.use('/api/v1', require('./routes/titans'))
app.use('/api/v1', require('./routes/characterRoute'))
app.use('/api/v1', require('./routes/users'))
app.use('/api/v1', require('./routes/auth'))

app.use('/api/v1', require('./routes/admin/titans'))
app.use('/api/v1', require('./routes/admin/characters'))

// Escuchar Servidor
const server = app.listen(process.env.PORT, () => {
  console.log(`Express server port: ${process.env.PORT}`);
})

module.exports = { app, server }