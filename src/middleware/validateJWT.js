const { response} = require('express')
const jwt = require('jsonwebtoken')

const validateJWT = (req, res = response, next) => {
  const token = req.header('x-auth-token')

  if (!token) {
    return res.status(401).json({
      success: false,
      msg: 'error'
    })
  }

  try {
    const { uid } = jwt.verify(token, process.env.JWT_SECRET)
    req.uid = uid
    next()
  } catch (error) {
    return res.status(401).json({
      success: false,
      msg: 'error'
    })
  }
}

module.exports = { validateJWT }