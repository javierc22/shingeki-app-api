const { Schema, model } = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

let validGenders = {
  values: ['male', 'female', 'other'],
  message: 'Error, expected {PATH} is not valid.'
}

const CharacterSchema = new Schema ({
  name: { type: String, required: true },
  lastname: { type: String, required: true },
  gender: { type: String, enum: validGenders, default: 'male', required: true },
  imageUrl: { type: String, required: false },
  extract: { type: String, required: false, default: "" },
  description: { type: String, required: false, default: "" },
  active: { type: Boolean, required: true, default: true }
}, { timestamps: true })

CharacterSchema.plugin(mongoosePaginate)

CharacterSchema.method('toJSON', function() {
  const { __v, _id, ...object } = this.toObject()
  object.id = _id
  return object
});

module.exports = model('Character', CharacterSchema)
