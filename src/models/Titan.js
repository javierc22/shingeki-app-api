const { Schema, model } = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const TitanSchema = new Schema({
  name: { type: String, required: true},
  userName: { type: String, required: true},
  extract: { type: String, required: false},
  description: { type: String, required: false},
  height: { type: Number, required: true},
  imageUrl: { type: String, required: false, default: ''},
  active: { type: Boolean, required: true, default: true },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
}, { timestamps: true })

TitanSchema.methods.setImageUrl = function setImageUrl (filename) {
  const { APP_HOST, PORT } = process.env
  this.imageUrl = `${APP_HOST}:${PORT}/public/${filename}`
}

TitanSchema.plugin(mongoosePaginate)

TitanSchema.method('toJSON', function() {
  // Extrae _id del Objecto
  const { __v, _id, ...object } = this.toObject()
  // Agrega el campo "id" y le damos el valor de "_id".
  object.id = _id;
  // Retorna el nuevo objeto modificado
  return object;
});

module.exports = model('Titan', TitanSchema)