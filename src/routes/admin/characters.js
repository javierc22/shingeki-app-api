const { Router } = require('express')
const { check } = require('express-validator')
const charactersController = require('../../controllers/admin/charactersController')
const upload = require('../../helpers/storage')
const { validateFields } = require('../../middleware/validateFields')
const { validateJWT } = require('../../middleware/validateJWT')

const router = Router()

// Todas las rutas tienen que pasar por la validación del JWT
router.use(validateJWT)

router.get('/admin/characters', charactersController.getCharacters)

router.post(
  '/admin/characters', 
  upload.single('image'), 
  [
    check('name','El nombre es obligatorio').not().isEmpty(),
    check('lastname','El apellido es obligatorio').not().isEmpty(),
    check('gender','El género es obligatorio').not().isEmpty(),
    check('gender', 'El género no es válido').isIn(['male', 'female', 'other']),
    validateFields
  ],
  charactersController.createCharacter
)

router.get('/admin/characters/:id', charactersController.getCharacter)

router.put(
  '/admin/characters/:id', 
  upload.single('image'),
  [
    check('name','El nombre es obligatorio').not().isEmpty(),
    check('lastname','El apellido es obligatorio').not().isEmpty(),
    check('gender','El género es obligatorio').not().isEmpty(),
    check('gender', 'El género no es válido').isIn(['male', 'female', 'other']),
    validateFields
  ],
  charactersController.updateCharacter
)

router.delete('/admin/characters/:id', charactersController.deleteCharacter)

module.exports = router