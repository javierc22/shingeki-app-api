const { Router } = require('express')
const { check } = require('express-validator')
const { getTitans, createTitan, getTitan, updateTitan, deleteTitan } = require('../../controllers/admin/titansController')
const upload = require('../../helpers/storage')
const { validateFields } = require('../../middleware/validateFields')
const { validateJWT } = require('../../middleware/validateJWT')

const router = Router()

// Todas las rutas tienen que pasar por la validación del JWT
router.use(validateJWT)

router.get('/admin/titans', getTitans)

router.post(
  '/admin/titans', 
  upload.single('image'), 
  [
    check('name','El nombre es obligatorio').not().isEmpty(),
    check('userName','El nombre portador es obligatorio').not().isEmpty(),
    check('height','La altura es obligatoria').not().isEmpty(),
    validateFields
  ],
  createTitan
)

router.get('/admin/titans/:id', getTitan)

router.put(
  '/admin/titans/:id', 
  upload.single('image'),
  [
    check('name','El nombre es obligatorio').not().isEmpty(),
    check('userName','El nombre portador es obligatorio').not().isEmpty(),
    check('height','La altura es obligatoria').not().isEmpty(),
    validateFields
  ],
  updateTitan
)

router.delete('/admin/titans/:id', deleteTitan)

module.exports = router