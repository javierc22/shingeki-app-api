const { Router } = require('express')
const { loginUser, authenticatedUser } = require('../controllers/authController')
const { validateJWT } = require('../middleware/validateJWT')

const router = Router()

router.post('/admin/sign_in', loginUser)

router.get('/admin/auth', validateJWT, authenticatedUser)

module.exports = router