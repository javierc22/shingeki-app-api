const { Router } = require('express')
const { getCharacters, createCharacter, getCharacter, updateCharacter } = require('../controllers/charactersController')

const router = Router()

router.get('/characters', getCharacters)

router.post('/characters', createCharacter)

router.get('/characters/:id', getCharacter)

router.put('/characters/:id', updateCharacter)

module.exports = router