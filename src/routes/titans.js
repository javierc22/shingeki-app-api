const { Router } = require('express')
const { getTitans, getTitan } = require('../controllers/titansController')

const router = Router()

router.get('/titans', getTitans)

router.get('/titans/:id', getTitan)


module.exports = router