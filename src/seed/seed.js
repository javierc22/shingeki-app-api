const fs = require("fs");
const mongoose = require("mongoose");
const { encryptPassword } = require('../helpers/encryptPassword')
const Titan = require("../models/Titan");
const User = require("../models/User");
require('dotenv').config()

// Connect to DB
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});

const userData = {
  _id: "607644f8c0cf6c5f113e82bf",
  name: "user",
  lastname: "admin",
  email: "admin@email.cl",
  password: encryptPassword("admin1234")
}


const titansData = JSON.parse(
  fs.readFileSync(`${__dirname}/data.json`, "utf-8")
)

// Import into DB
const importData = async () => {
  try {
    await User.create(userData)
    await Titan.create(titansData)
    console.log("Data Imported...");
    process.exit();
  } catch (err) {
    console.error(err);
  }
}

// Delete data
const deleteData = async () => {
  try {
    await User.deleteMany()
    await Titan.deleteMany()
    console.log("Data Destroyed...");
    process.exit();
  } catch (err) {
    console.error(err);
  }
}

if (process.argv[2] === "-i") {
  importData();
} else if (process.argv[2] === "-d") {
  deleteData();
}
