const Titan = require('../../src/models/Titan')
const User = require('../../src/models/User')
const jwt = require('jsonwebtoken')
const { api } = require('../helpers/api')
const { newUser } = require('../helpers/titans')

beforeEach( async() =>{
  await Titan.deleteMany({})
  await User.deleteMany({})
})

describe('GET /api/v1/admin/titans', () => {
  test('Respond with Status Code 401', async() => {
    await api
      .get("/api/v1/admin/titans")
      .expect("Content-Type", /application\/json/)
      .expect(401)
  })

  test('Respond with Status Code 200', async() => {
    const jwtSpy = jest.spyOn(jwt, 'verify')
    jwtSpy.mockReturnValue('decodedtoken');

    await api
      .get("/api/v1/admin/titans")
      .set('x-auth-token', 'jwttoken12345')
      .expect("Content-Type", /application\/json/)
      .expect(200)
  })
})

describe('POST /api/v1/admin/titans', () => {
  test('Add a valid Titan', async() => {
    const userObject = new User(newUser)
    const userTest = await userObject.save()
    
    const jwtSpy = jest.spyOn(jwt, 'verify')
    jwtSpy.mockReturnValue({ uid: userTest.id });

    const newTitan = {
      name: 'Titán 1',
      userName: 'Usuario Portador 1',
      extract: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      description: 'Praesent varius dolor massa, eu semper justo finibus eu',
      height: 23
    }

    const response = await api
      .post('/api/v1/admin/titans').send(newTitan)
      .set('x-auth-token', 'jwt12345')

      console.log(response.body)
  })
})

