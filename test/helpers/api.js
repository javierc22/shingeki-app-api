const supertest = require('supertest')
const { app } = require('../../src/index')

const api = supertest(app)

module.exports = { api }