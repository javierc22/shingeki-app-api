const newUser = {
  name: "Usuario 1",
  lastname: "Testing",
  email: "test@test.com",
  password: "Hola1234",
  created_at: Date.now()
}

const initialTitans = [
  {
    name: 'Titán 1',
    userName: 'Usuario Portador 1',
    extract: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent varius dolor massa, eu semper justo finibus eu',
    height: 23
  },
  {
    name: 'Titán 2',
    userName: 'Usuario Portador 2',
    extract: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent varius dolor massa, eu semper justo finibus eu',
    height: 27
  },
]

module.exports = {
  initialTitans,
  newUser
}