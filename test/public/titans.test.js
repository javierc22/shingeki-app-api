const Titan = require('../../src/models/Titan')
const User = require('../../src/models/User')
const { initialTitans, newUser } = require('../helpers/titans')
const { api } = require('../helpers/api')

beforeEach(async () =>{
  await Titan.deleteMany({})
  await User.deleteMany({})

  // parallel
  // const titans = initialTitans.map(titan => new Titan(titan))
  // const promises = titans.map(titan => titan.save())
  // await Promise.all(promises)

  // Secuential
  const userObject = new User(newUser)
  const userTest = await userObject.save()
  
  for ( const titan of initialTitans) {
    const titanObject = new Titan(titan)
    titanObject.user = userTest
    await titanObject.save()
  }
})

describe('GET /api/v1/titans', () => {
  test('respond with status code 200', async() => {
    await api
      .get("/api/v1/titans")
      .set("Accept", "application/json")
      .expect("Content-Type", /application\/json/)
      .expect(200)
  })

  test('respond with JSON containing a list of all titans', async() => {
    const response = await api.get("/api/v1/titans")

    expect(response.statusCode).toBe(200)
    expect(response.body.success).toBe(true)
    expect(response.body.titans).toHaveLength(initialTitans.length)
  })

  test('the first titans is about Titan 1 y Titan 2', async() => {
    const response = await api.get("/api/v1/titans")

    const contents = response.body.titans.map(titan => titan.name)
    expect(contents).toContain("Titán 1")
    expect(contents).toContain("Titán 2")
  })
})