const mongoose = require('mongoose')
const { server } = require('../src/index')

beforeAll( async() => {
  const connectionString = process.env.MONGO_URI_TEST;

  const connectionOptions = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  }

  await mongoose.connect(connectionString, connectionOptions)
})

// Disconnect Mongoose
afterAll( async() => {
  await mongoose.connection.close()
  await server.close()
})